<?php

namespace StudyCrack;

class User
{
	public static function select($id)
	{
		if (self::exists($id)) {
			return new self($id);
		}

		return false;
	}

	private function __construct($id)
	{
        $this->id           = $id;

		$mysql              = \User::find($id);
		$this->username     = $mysql->username;
		$this->name         = $mysql->name;

		$this->friend_count = (integer) \RedisL4::connection('users')->hget($this->id, 'friend_count');
		$this->friends      = (array) \RedisL4::connection('users')->smembers($this->id . ':friends');

		$this->exam_count   = (integer) \RedisL4::connection('users')->hget($this->id, 'exam_count');
		$this->exams        = (array) \RedisL4::connection('users')->smembers($this->id . ':exams');
	}

	public static function exists($id)
	{
		return (boolean) \User::find($id);
	}

	public static function friend($from, $to)
	{
		if (!self::exists($from)) return false;
		if (!self::exists($to)) return false;
		if ($from == $to) return false;
		if (self::are_friends($from, $to)) return true;

		$redis = \RedisL4::connection('users');

		$redis->hincrby($from, 'friend_count', 1);
		$redis->hincrby($to, 'friend_count', 1);

		$redis->sadd($from . ':friends', $to);
		$redis->sadd($to . ':friends', $from);

		Notification::send($from, $to, 'added_as_friend');

		return true;
	}

	public static function are_friends($from, $to)
	{
		if (!self::exists($from)) return false;
		if (!self::exists($to)) return false;
		if ($from == $to) return false;

		$redis = \RedisL4::connection('users');

		return (boolean) $redis->sismember($from . ':friends', $to) && $redis->sismember($to . ':friends', $from);
	}
}
