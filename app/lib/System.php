<?php

namespace StudyCrack;

class System
{
	public static function flush()
	{
		error_reporting(0);

		# MySQL...
		$tables = ['users', 'notifications', 'exams', 'questions'];

		foreach ($tables as $table) {
			\DB::table($table)->truncate();
		}

		# Redis...
		$databases = ['users', 'exams', 'notifications'];

		foreach ($databases as $database) {
			\RedisL4::connection($database)->flushdb();
		}

		return true;
	}
}
