<?php

namespace StudyCrack;

class Question
{
	public static function select($id)
	{
		if (self::exists($id)) {
			return new self($id);
		}

		return false;
	}

	public function __construct($id)
	{
		$this->id             = $id;

		$mysql                = \Question::find($id);
		$this->exam           = (integer) $mysql->exam;
		$this->question       = $mysql->question;
		$this->answers        = unserialize($mysql->answers);
		$this->correct_answer = $mysql->correct_answer;
	}

	public static function exists($id)
	{
		return (boolean) \Question::find($id);
	}

	public static function create($exam, $question, $answers, $correct_answer)
	{
		if (!Exam::exists($exam)) return false;

		$mysql                 = new \Question();
		$mysql->exam           = $exam;
		$mysql->question       = $question;
		$mysql->answers        = serialize($answers);
		$mysql->correct_answer = $correct_answer;
		$mysql->save();

		$redis = \RedisL4::connection('exams');
		$redis->hincrby($exam, 'question_count', 1);
		$redis->sadd($exam . ':questions', $mysql->id);

		return self::select((integer) $mysql->id);
	}

	public static function delete($id)
	{
		if (!self::exists($id)) return false;

		$mysql = \Question::find($id);
		$exam = (integer) $mysql->exam;
		$mysql->delete(); // Delete's from MySQL.

		$redis = \RedisL4::connection('exams');
		$redis->hincrby($exam, 'question_count', -1);
		$redis->srem($exam . ':questions', $id);

		return true;
	}
}
