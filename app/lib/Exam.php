<?php

namespace StudyCrack;

class Exam
{
	public static function select($id)
	{
		if (self::exists($id)) {
			return new self($id);
		}

		return false;
	}

	private function __construct($id)
	{
		$this->id             = $id; // We know it exists; we checked it in the `select` method.

		$mysql                = \Exam::find($id);
		$this->name           = $mysql->name;

		$redis                = \RedisL4::connection('exams');
		$this->question_count = (integer) $redis->hget($id, 'question_count');
		$this->questions      = $this->get_questions();
		$this->buddy_count    = (integer) $redis->hget($id, 'buddy_count');
		$this->buddies        = $this->buddy_count > 0 ? array_map('intval', $redis->smembers($id . ':buddies')) : null;
	}

	private function get_questions()
	{
		$questions = $this->question_count > 0 ? array_map('intval', \RedisL4::connection('exams')->smembers($this->id . ':questions')) : null;
		if (is_null($questions)) return null;

		sort($questions);
		return $questions;
	}

	public static function exists($id)
	{
		return (boolean) \Exam::find($id);
	}

	public static function create($name)
	{
		$mysql         = new \Exam();
		$mysql->name   = $name;
		$mysql->save();

		return true;
	}

	public function add_questions($quizlet_id)
	{
		$client = $client = new \GuzzleHttp\Client();

		$response = $client->get('https://api.quizlet.com/2.0/sets/'. $quizlet_id .'/terms', [
			'query' => [
				'client_id' => 'mumW6eFtFD'
			]
		]);

		$terms = json_decode($response->getBody(), true);
		$answer_choices = ['A', 'B', 'C', 'D'];
		$questions = [];

		foreach ($terms as $key => $value) {
			$questions[] = self::add_question(substr($terms[$key]['definition'], 0, -1) . '?', ['A' => $terms[$key]['term'], 'B' => $terms[$key]['term'], 'C' => $terms[$key]['term'], 'D' => $terms[$key]['term']], $answer_choices[rand(0, 3)])->id;
		}
	}

	public function remove_extraneous()
	{
		foreach ($this->questions as $question_id) {
			$question = Question::select($question_id);

			switch (explode(' ', $question->answers[$question->correct_answer])[1]) {
				case 'n':
				case 'v':
				case 'adj':
					continue;
					break;

				default:
					Question::delete((integer) $question_id);
					break;
			}
		}
	}

	public function randomize_answers()
	{
		foreach ($this->questions as $question_id) {
			$question = Question::select($question_id);

			$mysql = \Question::find($question_id);
			$mysql->answers = serialize($this->get_other_answers($this->questions, $question->answers, $question->correct_answer, $question_id));
			$mysql->save();
		}

		return true;
	}

	public function get_other_answers($ids, $answers, $correct_answer, $question_id)
	{
		if ($correct_answer != 'A') $answers['A'] = $this->get_other_answer($ids, $correct_answer, $question_id);
		if ($correct_answer != 'B') $answers['B'] = $this->get_other_answer($ids, $correct_answer, $question_id);
		if ($correct_answer != 'C') $answers['C'] = $this->get_other_answer($ids, $correct_answer, $question_id);
		if ($correct_answer != 'D') $answers['D'] = $this->get_other_answer($ids, $correct_answer, $question_id);

		return $answers;
	}

	public function get_other_answer($ids, $correct_answer, $question_id)
	{
		$other_question = Question::select(array_rand($ids));
		$other_answer = $other_question->answers['A'];
		$question = Question::select($question_id);

		if (explode(' ', $other_answer)[1] == explode(' ', $question->answers[$question->correct_answer])[1]) { // Part of speech must match.
			return $other_answer;
		}

		return $this->get_other_answer($ids, $correct_answer, $question_id);
	}

	public function add_question($question, $answers, $correct_answer)
	{
		return Question::create($this->id, $question, $answers, $correct_answer);
	}
}
