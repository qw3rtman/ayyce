<?php

namespace StudyCrack;

class Account
{
	public static function id()
	{
		if (is_null(\Auth::id())) return null;

		return (int) \Auth::id();
	}

	public static function check()
	{
		return (boolean) \Auth::check();
	}

	public static function create($desired_username, $password, $name)
	{
		$mysql = new \User();
		$mysql->username = $desired_username;
		$mysql->password = \Hash::make($password);
		$mysql->name = $name;
		$mysql->save();

		\Auth::loginUsingId((int) $mysql->id);

		return true;
	}

	public static function login($username, $password)
	{
		return \Auth::attempt(['username' => $username, 'password' => $password]);
	}

	public static function get_all_notifications()
	{
		if (!self::check()) return false;

		$unread = self::get_all_unread_notifications();
		$read = self::get_all_read_notifications();

		if (!is_array($unread) && is_array($read)) return $read;
		if (is_array($unread) && !is_array($read)) return $unread;
		if (!is_array($unread) && !is_array($read)) return null; // NULL denotes emtpy set.

		$all = array_map('intval', array_merge($unread, $read));

		return $all;
	}

	public static function get_all_unread_notifications()
	{
		if (!self::check()) return false;

		$unread = array_map('intval', \RedisL4::connection('notifications')->lrange(self::id() . ':unread', 0, -1));

		if (empty($unread)) return null;
		return $unread;
	}

	public static function get_all_read_notifications()
	{
		if (!self::check()) return false;

		$read = array_map('intval', \RedisL4::connection('notifications')->lrange(self::id() . ':read', 0, -1));

		if (empty($read)) return null;
		return $read;
	}
}
