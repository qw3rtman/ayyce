<?php

namespace StudyCrack;

class Notification
{
	public static $valid_types = ['added_as_friend'];

	public static function select($id)
	{
		if (self::exists($id)) {
			return new self($id);
		}

		return false;
	}

	private function __construct($id)
	{
		$this->id           = $id;

		$mysql              = \Notification::find($id);
		$this->from         = (integer) $mysql->from;
		$this->to           = (integer) $mysql->to;
		$this->body         = (array) json_decode($mysql->body, true);
		$this->is_dismissed = (boolean) $mysql->is_dismissed;
		$this->created_at   = $mysql->created_at;
	}

	public static function exists($id)
	{
		return (boolean) \Notification::find($id);
	}

	public static function send($from, $to, $type, $body = null)
	{
		if (!User::exists($from)) return false;
		if (!User::exists($to)) return false;
		if (!self::type_works($type)) return false;
		if (!self::body_works($type, $body)) return false;

		$mysql                  = new \Notification();
		$mysql->from            = $from;
		$mysql->to              = $to;
		$mysql->type            = $type;
		if ($body) $mysql->body = json_encode($body);
		$mysql->save();

		\RedisL4::connection('notifications')->lpush($to . ':unread', $mysql->id);

		return true;
	}

	public static function dismiss($id)
	{
		if (!self::exists($id)) return false;

		$mysql = \Notification::find($id);
		$mysql->is_dismissed = true;
		$mysql->save();

		$to = $mysql->to; // Recipient of notification.

		$redis = \RedisL4::connection('notifications');
		$redis->lrem($to . ':unread', 1, $id); // Removes from unread...
		$redis->lpush($to . ':read', $id); // Pushes into read...
		$redis->ltrim($to . ':read', 0, 49); // Caps number of read notifications to 50.

		return true;
	}

	private static function type_works($type)
	{
		return (boolean) in_array($type, self::$valid_types);
	}

	private static function body_works($type, $body)
	{
		switch ($type) {
			case 'added_as_friend':
				return true;
				break;
		}

		return false;
	}
}
