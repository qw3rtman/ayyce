<?php

class Exam extends Eloquent
{
	protected $table = 'exams';

	public $timestamps = false;
}
